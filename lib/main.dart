import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MobileScaffold(),
    );
  }
}

class MobileScaffold extends StatefulWidget {
  const MobileScaffold({Key? key}) : super(key: key);

  @override
  State<MobileScaffold> createState() => ;
}

var myDefaultBackground = Colors.grey[300];

var myAppBar = AppBar(
  backgroundColor: Colors.grey[900],
);

var myDrawer = Drawer(
  backgroundColor: Colors.grey[300],
  child: Column(
    children: [
      DrawerHeader(
        child: Icon(Icons.person, size: 50,),
      ),
      ListTile(
        leading: Icon(Icons.home),
        title: Text('MAINPAGE'),
      ),
      ListTile(
        leading: Icon(Icons.backup_table),
        title: Text('CLASS SCHEDULE'),
      ),
      ListTile(
        leading: Icon(Icons.badge),
        title: Text('PERSONAL'),
      ),
      ListTile(
        leading: Icon(Icons.attach_money),
        title: Text('COST BURDEN'),
      ),
      ListTile(
        leading: Icon(Icons.logout),
        title: Text('L O G O U T'),
      ),
    ],
  ),
);